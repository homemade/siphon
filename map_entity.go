package siphon

import "fmt"

type mapEntity struct {
	name string
	data map[string]interface{}
}

func NewMapEntity(name string, input interface{}) (e Entity, err error) {

	me := &mapEntity{name: name}

	if input == nil {
		me.data = make(map[string]interface{})
		return me, nil
	}

	data, ok := input.(map[string]interface{})

	if !ok {
		return nil, fmt.Errorf("Can't convert input to mapEntity")
	}

	me.data = data

	return me, nil
}

func (m *mapEntity) Name() string {
	return m.name
}

func (m *mapEntity) Get(field string) (interface{}, error) {

	v, ok := m.data[field]

	if !ok {
		return nil, nil
	}

	return v, nil
}
