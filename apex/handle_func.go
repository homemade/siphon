package apex

import (
	"encoding/json"

	"bitbucket.org/homemade/siphon"
	"github.com/apex/go-apex"
)

type Handler interface {
	Handle(siphon.Entity, *siphon.Response, *Context) error
}

type HandlerFunc func(siphon.Entity, *siphon.Response, *Context) error

func (h HandlerFunc) Handle(e siphon.Entity, r *siphon.Response, c *Context) error {
	return h(e, r, c)
}

// Handle Lambda events with the given handler, the same as Apex
func Handle(h Handler) {

	apex.HandleFunc(func(event json.RawMessage, ctx *apex.Context) (interface{}, error) {

		// Process response
		var req siphon.Request

		if err := json.Unmarshal(event, &req); err != nil {
			return nil, err
		}

		// Extract entity
		entity, err := siphon.NewMapEntity(req.EntityName, req.Entity)

		if err != nil {
			return nil, err
		}

		// Make a contenxt
		sctx := &Context{
			Context: ctx,
			Request: &req,
		}

		// Generate a response
		resp := siphon.NewResponse()

		// Make the function call
		if err := h.Handle(entity, resp, sctx); err != nil {
			return nil, err
		}

		return resp, nil
	})
}

// HandleFunc handles Lambda events with the given handler function, the same as Apex .
func HandleFunc(h HandlerFunc) {
	Handle(h)
}
