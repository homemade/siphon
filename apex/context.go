package apex

import (
	"bitbucket.org/homemade/siphon"
	"github.com/apex/go-apex"
)

type Context struct {
	*apex.Context

	Request *siphon.Request
}
