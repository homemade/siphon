package siphon

type Entity interface {
	Name() string
	Get(field string) (interface{}, error)
}
