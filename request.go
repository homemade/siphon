package siphon

type Request struct {
	EntityName string      `json:"entityName,omitempty"`
	Entity     interface{} `json:"entity,omitempty"`
}
