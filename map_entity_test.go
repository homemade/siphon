package siphon

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func validMapEntity(t *testing.T, name string, data ...map[string]interface{}) (me *mapEntity) {

	me = &mapEntity{}
	me.name = name

	if len(data) > 0 {
		me.data = data[0]
	} else {
		me.data = make(map[string]interface{})
	}

	return me
}

func Test_AMapEntityCanBeCreatedFromArbitraryDataOrTheFunctionGivesAnError(t *testing.T) {

	for cycle, input := range []struct {
		input  interface{}
		entity *mapEntity
		err    error
	}{
		// Nil input: no error expected (map should be created)
		{
			entity: validMapEntity(t, "test0"),
		},

		// Valid map: no error
		{
			input:  map[string]interface{}{},
			entity: validMapEntity(t, "test1"),
		},

		// Valid map with content: no error, content included
		{
			input:  map[string]interface{}{"test0": "test1"},
			entity: validMapEntity(t, "test2", map[string]interface{}{"test0": "test1"}),
		},

		// Invalid map: error expected
		{
			input: map[string]int{},
			err:   fmt.Errorf("Can't convert input to mapEntity"),
		},

		// Map request from JSON: no error expected
		{
			input:  validRequestJsonCycle(t, validRequest(t)).Entity,
			entity: validMapEntity(t, "test4"),
		},

		// Map request from JSON with content: no error expected, content included
		{
			input: validRequestJsonCycle(t, Request{
				Entity: map[string]interface{}{"test0": "test1"},
			}).Entity,
			entity: validMapEntity(t, "test5", map[string]interface{}{"test0": "test1"}),
		},
	} {
		t.Logf("Cycle %d", cycle)

		en, err := NewMapEntity(fmt.Sprintf("test%d", cycle), input.input)

		require.Equal(t, input.err, err)

		if input.err == nil {
			require.Equal(t, input.entity, en.(*mapEntity))
		}
	}
}

func Test_AMapEntityCanGetItsNameAndValues(t *testing.T) {

	for cycle, input := range []struct {
		entity *mapEntity
		values map[string]interface{}
	}{
		{
			entity: validMapEntity(t, "test0", map[string]interface{}{"test0": "test1"}),
			values: map[string]interface{}{"test0": "test1"},
		},
		{
			entity: validMapEntity(t, "test1", map[string]interface{}{"test0": 0, "test1": 1}),
			values: map[string]interface{}{"test0": 0, "test1": 1},
		},
	} {
		t.Logf("Cycle %d", cycle)

		require.Equal(t, fmt.Sprintf("test%d", cycle), input.entity.Name())

		for k, v := range input.values {
			ret, err := input.entity.Get(k)
			require.Nil(t, err)
			require.Equal(t, v, ret)
		}
	}
}
