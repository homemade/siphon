package siphon

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
)

func validRequest(t *testing.T) Request {
	return Request{
		Entity: map[string]interface{}{},
	}
}

func validRequestAsJson(t *testing.T, template ...Request) []byte {

	r := validRequest(t)

	if len(template) > 0 {
		r = template[0]
	}

	jsn, err := json.Marshal(&r)
	require.Nil(t, err)

	return jsn
}

func validRequestFromJson(t *testing.T, jsn []byte) (r Request) {

	require.Nil(t, json.Unmarshal(jsn, &r))

	return
}

func validRequestJsonCycle(t *testing.T, input Request) (output Request) {

	return validRequestFromJson(t, validRequestAsJson(t, input))
}
