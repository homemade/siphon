package siphon

import "fmt"

type Response struct {
	UpdatedEntity     map[string]interface{} `json:"updatedEntity,omitempty"`
	EntityFieldErrors map[string][]string    `json:"entityFieldErrors,omitempty"`
	Errors            []string               `json:"errors,omitempty"`
}

func NewResponse() *Response {
	return &Response{
		UpdatedEntity:     make(map[string]interface{}),
		EntityFieldErrors: make(map[string][]string),
	}
}

func (r *Response) AddError(s string, args ...interface{}) {
	r.Errors = append(r.Errors, fmt.Sprintf(s, args...))
}

func (r *Response) AddFieldError(field, s string, args ...interface{}) {

	if _, ok := r.EntityFieldErrors[field]; !ok {
		r.EntityFieldErrors[field] = []string{}
	}

	r.EntityFieldErrors[field] = append(r.EntityFieldErrors[field], fmt.Sprintf(s, args...))
}

func (r *Response) SetFieldUpdate(field string, value interface{}) {
	r.UpdatedEntity[field] = value
}
